import React from "react";
import ReactDOM from "react-dom";
import Site from "./Site";
import "./css/main.css";

ReactDOM.render(<Site />, document.getElementById("dynamic-content"));
