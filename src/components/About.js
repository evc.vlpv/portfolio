import React, { Component } from "react";
import TitlePrimary from "./commons/TitlePrimary";
import TitleSecondary from "./commons/TitleSecondary";
import Menu from "./commons/Menu";
import Footer from "./commons/Footer";
import portrait from "../img/evc.png";
import DetailBox from "./commons/DetailBox";

class About extends Component {
  render() {
    return (
      <div>
        <Menu
          menuOptions={this.props.menuOptions}
          handler={this.props.navigationHandler}
          current="About"
        />
        <main id="about">
          <TitlePrimary mainText="About " secondaryText="Me" />
          <TitleSecondary text="Let me tell you a few things about me" />
          <div className="about-info">
            <img src={portrait} alt="Enrique Valverde" className="bio-image" />
            <div className="bio">
              <h3 className="text-secondary">BIO</h3>
              <p>
                Software Engineer with a bachelor degree in Computer Science,
                strong leadership and research skills. Over 8 years of software
                development experience, with emphasis on Java and Javascript web
                development technologies. <br />
                My other skills include fast learning, goal achievement focus,
                teamwork, self-motivation. <br />
                Specialties in Software development: OOP, Scrum, Design
                Patterns, MVC, Web Services, Responsive Design.
              </p>
            </div>
            <div className="tech-stack">
              <p>
                <i className="fab fa-java fa-2x" />
                <i className="fab fa-js-square fa-2x" />
                <i className="fab fa-react fa-2x" />
                <i className="fab fa-html5 fa-2x" />
                <i className="fab fa-css3-alt fa-2x" />
                <i className="fab fa-sass fa-2x" />
                <i className="fab fa-git fa-2x" />
                <i className="fab fa-npm fa-2x" />
              </p>
            </div>
            <DetailBox
              classes="job job-1"
              identifier="job-1"
              title="Oficina de Divulgación e Información UCR"
              subtitle="Front End Developer"
              extraInfo="2009-2011"
              paragraph={[
                "My major role was to manage and update the content of the official website.",
                <br />,
                "The technologies I worked with:"
              ]}
              items={["HTML", "CSS", "CMS"]}
            />
            <DetailBox
              classes="job job-2"
              identifier="job-2"
              title="Orgánico Soft"
              subtitle="Back End Developer"
              extraInfo="2011-2012"
              paragraph={[
                "Orgánico Soft was the software department, at an Online Sports Betting Company. While in there, I helped in all server-side related tasks.",
                <br />,
                "From DB reporting to new features development, and also legacy software maintenance.",
                <br />,
                "Technologies:"
              ]}
              items={["J2EE", ".NET C#", "SQL Server", "Oracle", "SSRS"]}
            />
            <DetailBox
              classes="job job-3"
              identifier="job-3"
              title="Alivebox"
              subtitle="Full Stack Developer"
              extraInfo="2012-2018"
              paragraph={[
                "During the last 7 years, I'd worked doing estimation, analysis, design, coding and unit testing for outsource projects. In both Front End and Back End.",
                <br />,
                "Technologies:"
              ]}
              items={[
                "J2EE",
                "Spring",
                "Hibernate",
                "Oracle",
                "MySQL",
                "Javascript",
                "JQuery",
                "HTML5",
                "CSS3",
                "ReactJS",
                "Bootstrap",
                "SASS"
              ]}
            />
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default About;
