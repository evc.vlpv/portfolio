import React, { Component } from "react";
import TitlePrimary from "./commons/TitlePrimary";
import TitleSecondary from "./commons/TitleSecondary";
import Menu from "./commons/Menu";
import Footer from "./commons/Footer";

class Home extends Component {
  render() {
    return (
      <div>
        <Menu
          menuOptions={this.props.menuOptions}
          handler={this.props.navigationHandler}
          current="Home"
        />
        <main id="home">
          <TitlePrimary mainText="Enrique " secondaryText="Valverde" />
          <TitleSecondary text="Full Stack Developer" />
          <div className="icons">
            <a
              href="https://gitlab.com/evc.vlpv/portfolio#README"
              target="_blank"
            >
              <i className="fab fa-gitlab fa-2x" />
            </a>
            <a
              href="https://www.linkedin.com/in/enrique-valverde-chac%C3%B3n-315216174/"
              target="_blank"
            >
              <i className="fab fa-linkedin fa-2x" />
            </a>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default Home;
