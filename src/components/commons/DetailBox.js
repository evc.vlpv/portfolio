import React, { Component } from "react";

class DetailBox extends Component {
  render() {
    const items = this.props.items.map((item, index) => {
      const identifier = this.props.identifier + "-" + index;
      return <li key={identifier}>{item}</li>;
    });
    return (
      <div className={this.props.classes}>
        <h3>{this.props.title}</h3>
        <h6>{this.props.subtitle}</h6>
        <h6>{this.props.extraInfo}</h6>
        <p>
          {this.props.paragraph}
          <ul>{items}</ul>
        </p>
      </div>
    );
  }
}

export default DetailBox;
