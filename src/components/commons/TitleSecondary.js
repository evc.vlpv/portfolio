import React, { Component } from "react";

class TitleSecondary extends Component {
  render() {
    return <h2 className="sm-heading">{this.props.text}</h2>;
  }
}

export default TitleSecondary;
