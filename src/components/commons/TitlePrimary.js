import React, { Component } from "react";

class TitlePrimary extends Component {
  render() {
    return (
      <h1 className="lg-heading">
        {this.props.mainText}
        <span className="text-secondary">{this.props.secondaryText}</span>
      </h1>
    );
  }
}

export default TitlePrimary;
