import React, { Component } from "react";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false
    };
  }

  componentDidMount() {
    this.menuBtn = document.querySelector(".menu-btn");
    this.menu = document.querySelector(".menu");
    this.menuNav = document.querySelector(".menu-nav");
    this.menuBranding = document.querySelector(".menu-branding");
    this.navItems = document.querySelectorAll(".nav-item");
    this.menuBtn.addEventListener("click", this.toggleMenu);
    this.navItems.forEach((item, index) => {
      const delay = (index + 1) * 0.1 + "s";
      item.style.transitionDelay = delay;
    });
  }

  componentWillUnmount() {
    this.menuBtn.removeEventListener("click", this.toggleMenu);
  }

  toggleMenu = () => {
    if (!this.state.showMenu) {
      this.menuBtn.classList.add("close");
      this.menu.classList.add("show");
      this.menuNav.classList.add("show");
      this.menuBranding.classList.add("show");
      this.navItems.forEach(item => item.classList.add("show"));
      this.setState({
        showMenu: true
      });
    } else {
      this.menuBtn.classList.remove("close");
      this.menu.classList.remove("show");
      this.menuNav.classList.remove("show");
      this.menuBranding.classList.remove("show");
      this.navItems.forEach(item => item.classList.remove("show"));
      this.setState({
        showMenu: false
      });
    }
  };

  render() {
    const options = this.props.menuOptions.map((option, index) => {
      const listIdentifier = "li-" + index;
      const anchorIdentifier = "a-" + index;
      const classes =
        option.label === this.props.current ? "nav-item current" : "nav-item";
      return (
        <li className={classes} key={listIdentifier}>
          <button
            onClick={this.props.handler}
            name={option.pageKey}
            key={anchorIdentifier}
            className="nav-link"
          >
            {option.label}
          </button>
        </li>
      );
    });
    return (
      <header>
        <MenuBtn />
        <nav className="menu">
          <MenuBranding />
          <ul className="menu-nav">{options}</ul>
        </nav>
      </header>
    );
  }
}

const MenuBtn = props => (
  <div className="menu-btn">
    <div className="btn-line" />
    <div className="btn-line" />
    <div className="btn-line" />
  </div>
);

const MenuBranding = props => (
  <div className="menu-branding">
    <div className="portrait" />
  </div>
);

export default Menu;
