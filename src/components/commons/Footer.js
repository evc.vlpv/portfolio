import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer id="main-footer">
        Copyright <i className="fas fa-copyright" /> 2018
      </footer>
    );
  }
}

export default Footer;
