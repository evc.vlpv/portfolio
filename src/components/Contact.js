import React, { Component } from "react";
import TitlePrimary from "./commons/TitlePrimary";
import TitleSecondary from "./commons/TitleSecondary";
import Menu from "./commons/Menu";
import Footer from "./commons/Footer";

class Contact extends Component {
  render() {
    return (
      <div>
        <Menu
          menuOptions={this.props.menuOptions}
          handler={this.props.navigationHandler}
          current="Contact"
        />
        <main id="contact">
          <TitlePrimary mainText="Contact " secondaryText="Me" />
          <TitleSecondary text="This is how can you reach me..." />
          <div className="boxes">
            <div>
              <span className="text-secondary">Email: </span>
              evc.vlpv@gmail.com
            </div>
            <div>
              <span className="text-secondary">Phone: </span>
              (506) 8810-0183
            </div>
            <div>
              <span className="text-secondary">San José, </span>
              Costa Rica
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default Contact;
