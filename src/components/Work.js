import React, { Component } from "react";
import TitlePrimary from "./commons/TitlePrimary";
import TitleSecondary from "./commons/TitleSecondary";
import Menu from "./commons/Menu";
import Footer from "./commons/Footer";
import project1 from "../img/projects/001-project-sm.png";
import project2 from "../img/projects/002-project-sm.png";
import project3 from "../img/projects/003-project-sm.png";
import project4 from "../img/projects/004-project-sm.png";
import project5 from "../img/projects/005-project-sm.png";
import project6 from "../img/projects/006-project-sm.png";
import project7 from "../img/projects/007-project-sm.png";

class Work extends Component {
  render() {
    return (
      <div>
        <Menu
          menuOptions={this.props.menuOptions}
          handler={this.props.navigationHandler}
          current="Projects"
        />
        <main id="work">
          <TitlePrimary mainText="My " secondaryText="Work" />
          <TitleSecondary text="Check out some of the projects I've been in" />
          <div className="projects">
            <div className="item">
              <img src={project1} alt="Project" />
              <h3>News and Media Website</h3>
              <h6>HTML, CSS, Joomla</h6>
            </div>
            <div className="item">
              <img src={project2} alt="Project" />
              <h3>Online Sports Betting Website</h3>
              <h6>J2EE, .NET C#, SQL Server, Oracle, SSRS</h6>
            </div>
            <div className="item">
              <img src={project3} alt="Project" />
              <h3>Online Grocery Store</h3>
              <h6>J2EE, PrimeFaces, Oracle, Magento</h6>
            </div>
            <div className="item">
              <img src={project4} alt="Project" />
              <h3>Motor Company Website</h3>
              <h6>HTML5, CSS3, SASS, J2EE, Spring Framework</h6>
            </div>
            <div className="item">
              <img src={project5} alt="Project" />
              <h3>Foreign Traveling Planning System</h3>
              <h6>
                J2EE, JSP, Spring Framework, Hibernate, Javascript, CSS3,
                Oracle, JasperReports
              </h6>
            </div>
            <div className="item">
              <img src={project6} alt="Project" />
              <h3>Inventory Managment Restfull API</h3>
              <h6>
                J2EE, Spring Boot, Spring Framework, Spring Security,
                ElasticSearch, Oracle
              </h6>
            </div>
            <div className="item">
              <img src={project7} alt="Project" />
              <h3>Advertising Budget Management System</h3>
              <h6>
                J2EE, PrimeFaces, Spring Framework, Hibernate, Javascript, CSS3,
                Oracle, JasperReports
              </h6>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default Work;
