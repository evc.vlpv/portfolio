import React, { Component } from "react";
import Home from "./components/Home";
import About from "./components/About";
import Contact from "./components/Contact";
import Work from "./components/Work";

class Site extends Component {
  constructor() {
    super();
    this.state = {
      currentTab: "home"
    };
    this.menuOptions = [
      { label: "Home", pageKey: "home" },
      { label: "About", pageKey: "about" },
      { label: "Projects", pageKey: "work" },
      { label: "Contact", pageKey: "contact" }
    ];
  }

  showContentHandler = event => {
    this.showContentAction(event.target.name);
  };

  showContentAction = currentContentKey => {
    this.setState({
      currentTab: currentContentKey
    });
  };

  render() {
    const displayContent = {
      home: Home,
      about: About,
      contact: Contact,
      work: Work
    };
    const DisplayContent = displayContent[this.state.currentTab];
    return (
      <DisplayContent
        menuOptions={this.menuOptions}
        navigationHandler={this.showContentHandler}
      />
    );
  }
}

export default Site;
